# Bio-IT Beer Session: Literate Programming
## 7-Mar-2017

[__Markdown Syntax Reference__](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

## R Markdown

[R Markdown homepage](http://rmarkdown.rstudio.com)

### Bernd Klaus

#### Introduction to R Markdown

[repository of material demonstrated](https://github.com/b-klaus/reproducibility_tutorial)

- annotate with text formatted with Markdown syntax
- execute code blocks in R, python, bash, others?
- formulas/equations written with LaTeX
- HTML wherever Markdown syntax is insufficient

### Mike Smith

#### Bioconductor

[Homepage](http://bioconductor.org)

Tutorials/workflows written in R Markdown or _Sweave_ and rendered in HTML. Package authors are encouraged to submit these.

Bioconductor teamed up with [F1000 Research](http://f1000research.com/), to publish these workflows as journal articles - resulting in the [Bioconductor Channel](https://f1000research.com/channels/bioconductor).

Developed an authoring process - R Markdown that goes to HTML and a PDF formatted for the journal -> [`BiocWorkflowTools`](https://bioconductor.org/packages/release/bioc/html/BiocWorkflowTools.html) package for R.

- provides an R Markdown file as template for article
  - structure
  - author information
  - metadata fields
  - examples explaining syntax etc
- includes all other files required to build article PDF based on the R Markdown
- provides function to upload to [Overleaf](https://www.overleaf.com) for submission to f1000 Research

## Jupyter

- browser-based
- locally-hosted
- executable code blocks in many different languages
- text blocks written in Markdown
- interactive widgets
  - [Python-specific](http://ipywidgets.readthedocs.io/en/latest/examples/Using%20Interact.html)
  - [General Jupyter Declarative Widgets e.g. for R](http://jupyter.org/declarativewidgets/)

### Jaime Huerta-Cepas

- Use Jupyter to document analysis/figure generation for publications
- Also for writing tutorials

Example notebooks: http://etetoolkit.org/cookbook/ and https://github.com/jhcepas/emapper-benchmark/blob/master/benchmark_analysis.ipynb

### Toby Hodges

[Coding Club Mini-Tutorial Materials & Interactive Widget Example](https://github.com/tobyhodges/jupyter-tutorial)
